module Main where

import Control.Concurrent
import Control.Monad
import System.Hardware.WiringPi
import System.Environment
import qualified Data.Char as Char
import qualified Data.Map as Map
import qualified Data.List as List

data Light = On | Off
  deriving Show
data Morse = Dash | Dot | Space
  deriving Show

type MorseChar = [Morse]
type MorseString = [MorseChar]
type Lights = [Light]

charMap :: Map.Map Char [Morse]
charMap = Map.fromList [
    ( 'A' , [Dot, Dash] )
	, ( 'B' , [Dash, Dot, Dot, Dot] )
	, ( 'C' , [Dash, Dot, Dash, Dot] )
	, ( 'D' , [Dash, Dot, Dot] )
	, ( 'E' , [Dot] )
	, ( 'F' , [Dot, Dot, Dash, Dot] )
	, ( 'G' , [Dash, Dash, Dot] )
	, ( 'H' , [Dot, Dot, Dot, Dot] )
	, ( 'I' , [Dot, Dot] )
	, ( 'J' , [Dot, Dash, Dash, Dash] )
	, ( 'K' , [Dash, Dot, Dash] )
	, ( 'L' , [Dot, Dash, Dot, Dot] )
	, ( 'M' , [Dash, Dash] )
	, ( 'N' , [Dash, Dot] )
	, ( 'O' , [Dash, Dash, Dash] )
	, ( 'P' , [Dot, Dash, Dash, Dot] )
	, ( 'Q' , [Dash, Dash, Dot, Dash] )
	, ( 'R' , [Dot, Dash, Dot] )
	, ( 'S' , [Dot, Dot, Dot] )
	, ( 'T' , [Dash] )
	, ( 'U' , [Dot, Dot, Dash] )
	, ( 'V' , [Dot, Dot, Dot, Dash] )
	, ( 'W' , [Dot, Dash, Dash] )
	, ( 'X' , [Dash, Dot, Dot, Dash] )
	, ( 'Y' , [Dash, Dot, Dash, Dash] )
	, ( 'Z' , [Dash, Dash, Dot, Dot] )
	, ( ' ' , [Space] )
  ]

second :: Int
second = 1000000

timeUnit :: Int
timeUnit = quot second 5

dotLights = [On]
dashLights = replicate (3 * length dotLights) On
spaceLights = [Off]
letterEndLights = [Off, Off]

symbolEndLights = [Off]

dot = dotLights ++ symbolEndLights
dash = dashLights ++ symbolEndLights
space = spaceLights ++ symbolEndLights

outputPin :: Pin
outputPin = Gpio 3

flash :: Light -> IO ()
flash On = digitalWrite outputPin HIGH
flash Off = digitalWrite outputPin LOW

flashLights :: Lights -> IO ()
flashLights lights = do
  let flashLight light = flash light >> threadDelay timeUnit
  mapM_ flashLight lights


encodeChar :: Char -> MorseChar
encodeChar = (Map.!) charMap
  
encodeString :: String -> MorseString 
encodeString = map $ encodeChar . Char.toUpper 

morseStringToLight :: MorseString -> Lights
morseStringToLight chars =  map (\ mc -> map morseToLights mc >>= id) chars >>=  (\ ll -> ll ++ letterEndLights)
  where 
    morseToLights Dot = dot
    morseToLights Dash = dash
    morseToLights Space = space

flashString :: String -> IO ()
flashString = flashLights . morseStringToLight . encodeString


main :: IO ()
main = do
  args <- getArgs
  let (toPrint:rest) = args
  pinMode outputPin OUTPUT
  digitalWrite outputPin LOW
  forever $ flashString toPrint
